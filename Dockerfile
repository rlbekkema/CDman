FROM python:3.6-alpine3.7

WORKDIR /app
COPY requirements.txt /app/requirements.txt

RUN apk update
RUN apk add docker
RUN pip install -qqr /app/requirements.txt

COPY main.py /app/main.py
COPY slack.py /app/slack.py

EXPOSE 80

ENV PYTHONUNBUFFERED=0
ENV FLASK_APP=main.py

CMD ["flask", "run", "--host=0.0.0.0", "--port=80"]
