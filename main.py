import json
import os
import subprocess
import sys

from flask import Flask, request, Response

from slack import send_slack_message

app = Flask(__name__)

token_file = os.getenv('GITLAB_SECRET_TOKEN_FILE')
if token_file:
    SECRET_TOKEN = open(token_file, 'r').read()
else:
    SECRET_TOKEN = '1234'

# Looks like [[<project_id>, {"image": <img>, "name": <service_name>}],...]
services_file = os.getenv('GITLAB_SERVICES_FILE')
if services_file:
    SERVICES = dict(json.loads(open(os.getenv('GITLAB_SERVICES_FILE'), 'r').read()))
else:
    SERVICES = {5629546: {'image': 'registry.gitlab.com/rlbekkema/mariobot-ts', 'name': 'mariobot_mariobot'}}

SLACK_WEBHOOK = os.getenv('SLACK_WEBHOOK')


@app.route('/', methods=['POST'])
def gitlab_webhook():
    if request.headers['X-Gitlab-Token'] != SECRET_TOKEN:
        return Response(status='400 WRONG')

    data = request.get_json()

    if request.headers['X-Gitlab-Event'] == 'Job Hook':
        if data['build_stage'] == 'build' and data['build_status'] == 'success':
            service = SERVICES.get(data['project_id'])
            if service:
                subprocess.run(['docker', 'service', 'update', '-d', '--image', service['image'],
                                service['name']], stdout=sys.stdout, stderr=sys.stderr)

                send_slack_message(f"Docker service *{service['name']}* updated ({data['repository']['homepage']})",
                                   'server', SLACK_WEBHOOK)

    return Response("Thanks!", content_type='text/plain')
