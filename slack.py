import json

import requests


def send_slack_message(message, channel, webhook):
    """Send a slack message"""

    data = {
        'text': message,
        'username': 'cdman',
        'channel': channel,
    }

    resp = requests.post(webhook, data=json.dumps(data))
    resp.raise_for_status()
